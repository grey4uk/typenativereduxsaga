import {StyleSheet} from 'react-native';

export default StyleSheet.create({
    buttonBlock:{
        flex:1,
        flexDirection:'column',
        alignItems:'center'
    },
  loadButton: {
    backgroundColor: 'gray',
    width: 280,
    borderRadius: 10,
    alignItems: 'center',
    marginBottom: 10,
    marginTop:40
  },
  buttonTitle: {
    color: '#fff',
    margin: 20,
  },
  post: {
    width: 280,
    backgroundColor: 'grey',
    borderRadius: 10,
    padding: 10,
    marginBottom: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
