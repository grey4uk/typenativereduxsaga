import React, {useState, useEffect} from 'react';
import {Text, View, TouchableOpacity, FlatList, Image} from 'react-native';
import {useDispatch, useSelector} from 'react-redux';
import styles from './post.css';
import { RootState } from '../../../redux/rootReducer';
import { loadPosts } from '../../../redux/actions';

const Posts = () => {
  const state = useSelector((state: RootState) => state.postsReducer);
  const dispatch = useDispatch();
  const [allPosts, setAllPosts] = useState<any | null>([]);

  useEffect(() => {
    setAllPosts(state);
  }, [state]);

  const handleClick = () => {
   dispatch(loadPosts());
  };

  return (
    <View style={styles.buttonBlock}>
      <TouchableOpacity style={styles.loadButton} onPress={handleClick}>
        <Text style={styles.buttonTitle}>Load Posts</Text>
      </TouchableOpacity>
      <FlatList
        keyExtractor={(item: any) => item.id}
        data={allPosts}
        renderItem={({item}) => (
          <View style={{marginBottom: 40}}>
            <Text>{item.image}</Text>
            <Image
              source={{
                uri: `${item.image}`,
              }}
              style={{width: 300, height: 220}}
            />
          </View>
        )}
      />
    </View>
  );
};

export default Posts;
