import React, {useState} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {View, Alert} from 'react-native';
import styles from './styles.css';
import ModalComponent from './Modal';
import AddTaskForm from './AddTaskForm';
import TaskList from './TaskList';
import {Task} from '../../../redux/types';
import {RootState} from '../../../redux/rootReducer';
import {addTaskAction,editTaskAction,delTaskAction} from '../../../redux/actions';

const App: React.FC = (): JSX.Element => {
  const tasks = useSelector((state: RootState) => state.taskReducer);
  const dispatch = useDispatch();
  // console.log('tasks :>> ', tasks);
  
  const [task, setTask] = useState<Task | null>(null);

  const addTask = (value: string): void => {
    !value
      ? Alert.alert('Empty post')
      :dispatch(addTaskAction({text: value, id: Number(Date.now())}))
  };

  const delTask = (id: number): void => {
    dispatch(delTaskAction(id));
  };

  const editTask = (id: number, textValue: string): void => {
    dispatch(editTaskAction({text:textValue,id}))
    setTask(null);
  };

  const handleEdit = (el: Task): void => {
    setTask(el);
  };

  return (
   
      <View style={styles.wrapper}>
        {task && <ModalComponent task={task} editTask={editTask} />}
        <AddTaskForm addTask={addTask} title={"ADD TASK"} />
        <TaskList delTask={delTask} tasks={tasks} handleEdit={handleEdit} />
      </View>
   
  );
};

export default App;
