import React,{useState} from 'react';
import {TextInput,TouchableOpacity,Text, View, Keyboard} from 'react-native';
import styles from './styles.css';
import CustomText from '../../customTextComponent/customText';

interface Props {
    addTask(value:string):void,
    title:string
}

const AddTaskForm: React.FC<Props> = ({addTask,title}):JSX.Element => {
    const [value, setValue] = useState<string>('');

    return (
        <View style={{marginBottom:40}}>
        <TextInput
          style={styles.input}
          placeholder="Enter task"
          value={value}
          onChangeText={setValue}
        />
        <TouchableOpacity style={styles.buttonAdd} onPress={()=>(Keyboard.dismiss(),addTask(value),setValue(''))}>
          <View style={styles.buttonTitle}>
            <CustomText textType={'bold'}>{title}</CustomText>
            </View>
        </TouchableOpacity>
        </View>
    );
}

export default AddTaskForm;