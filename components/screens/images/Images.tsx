import React, {useEffect, useState} from 'react';
import {View, FlatList, Image, Text, TouchableOpacity} from 'react-native';
import Modal from 'react-native-modal';
import {useDispatch, useSelector} from 'react-redux';
import {RootState} from 'redux/rootReducer';
import {loadImages, saveImage} from '../../../redux/actions';
import AddTaskForm from '../app/AddTaskForm';
import styles from '../app/styles.css';

const Images = () => {
  const state = useSelector((state: RootState) => state.imagesReducer);
  const dispatch = useDispatch();
  const [allPosts, setAllPosts] = useState<any>([]);
  const [flag, setFlag] = useState<boolean>(false);

  useEffect(() => {
    setAllPosts(state);
  }, [state]);

  const getCollection = (value: string) => {
    dispatch(loadImages(value));
  };

  const savePhoto = async (image: string) => {
    await setFlag(true);
    const id = await setTimeout(() => {
      setFlag(false);
      clearTimeout(id);
    }, 2000);
    dispatch(saveImage(image));
  };

  return (
    <View style={{flex: 1, flexDirection: 'column', alignItems: 'center'}}>
      <AddTaskForm addTask={getCollection} title={'LOAD IMAGES'} />
      <FlatList
        keyExtractor={(item: any) => String(item.id)}
        data={allPosts}
        renderItem={({item}) => (
          <View style={{marginBottom: 40}}>
            <Text>{item.tags}</Text>
            <TouchableOpacity onLongPress={() => savePhoto(item.previewURL)}>
              <Image
                source={{
                  uri: `${item.previewURL}`,
                }}
                style={{width: 300, height: 220}}
              />
            </TouchableOpacity>
          </View>
        )}
      />
      <Modal isVisible={flag}>
        <View style={{flex: 1}}>
          <Text style={styles.modalInput}>SAVED TO POSTS</Text>
        </View>
      </Modal>
    </View>
  );
};

export default Images;
