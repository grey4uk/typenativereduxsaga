import React from 'react';
import {Provider} from 'react-redux';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import App from './screens/app/App';
import Images from './screens/images/Images';
import store from '../redux/store';
import Posts from './screens/posts/Posts';
import CustomText from './customTextComponent/customText';

const Tab = createBottomTabNavigator();

const index = () => {
  return (
    <Provider store={store}>
      <NavigationContainer>
        <Tab.Navigator
          initialRouteName="App"
          tabBarOptions={{
            labelStyle: {fontSize: 24},
            activeTintColor: 'grey',
            activeBackgroundColor: '#fff',
            inactiveTintColor: '#fff',
            inactiveBackgroundColor: 'grey',
          }}>
          <Tab.Screen
            name="App"
            component={App}
            options={{
              tabBarLabel: ()=><CustomText style={[{color:'red',fontSize:24}]}>App</CustomText>,
              // tabBarIcon: ({ color="#000", size=40 }) => (
              //   <Ionicons name="md-images" color={color} size={size}  />
              // ),
            }}
          />
          <Tab.Screen
            name="Images"
            component={Images}
            options={{
              tabBarLabel: 'Images',
              // tabBarIcon: ({ color="red", size=40 }) => (
              //   <Ionicons name="ios-add" color={color} size={size} />
              // ),
            }}
          />
          <Tab.Screen
            name="Posts"
            component={Posts}
            options={{
              tabBarLabel: 'Posts',
              // tabBarIcon: ({ color="red", size=40 }) => (
              //   <Ionicons name="ios-add" color={color} size={size} />
              // ),
            }}
          />
        </Tab.Navigator>
      </NavigationContainer>
      {/* <App/> */}
    </Provider>
  );
};

export default index;
