import React, { FC } from 'react'
import {
  StyleSheet,
  Text,
  TextStyle
} from 'react-native';

type CustomTextProps = {
    style?: TextStyle | TextStyle[],
    textType?: 'regular' | 'bold' 
  }

  const CustomText: FC<CustomTextProps> = ({ children, textType, style }) => {
    let textStyle: {}
    switch (textType) {
      case 'regular':
        textStyle = styles.regular
        break
      case 'bold':
        textStyle = styles.bold
        break
      default:
        textStyle = styles.regular
        break
    }

    const passedStyles = Array.isArray(style) ? Object.assign({}, ...style) : style

  return (
      <Text style={[textStyle, { ...passedStyles } ]}>
        {children}
      </Text>
    )
  }
  const styles = StyleSheet.create({
    regular: {
      fontFamily: 'IndieFlower-Regular',
      fontSize:18
    },
    bold: {
      fontFamily: 'IndieFlower-Regular',
      fontWeight:'bold',
      fontSize:18
    }
  })
  export default CustomText