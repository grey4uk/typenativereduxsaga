export interface Task {
  text: string;
  id: number;
}

export const ADD_TASK = '@ToDo/ADD';
export interface AddTask {
  type: typeof ADD_TASK;
  payload: Task;
}

export const EDIT_TASK = '@ToDo/EDIT';
export interface EditTask {
  type: typeof EDIT_TASK;
  payload: Task;
}

export const DELETE_TASK = '@ToDo/DELETE';
export interface DeleteTask {
  type: typeof DELETE_TASK;
  payload: number;
}

export const LOAD_IMAGES = '@Images/LOAD_IMAGES';
export interface LoadImages {
  type: typeof LOAD_IMAGES,
  payload:string
}

export const POST_IMAGES = '@Images/POST_IMAGES';
export interface PostImages {
  type: typeof POST_IMAGES,
  payload:string[]
}

export const SAVE_IMAGE = '@Images/SAVE_IMAGE';
export interface SaveImage {
  type: typeof SAVE_IMAGE,
  payload:string
}

export const LOAD_POSTS = '@Posts/LOAD_POSTS';
export interface LoadPosts {
  type: typeof LOAD_POSTS
}

export const POST_POSTS = '@Posts/POST_POSTS';
export interface PostPosts {
  type: typeof POST_POSTS,
  payload:[]
}

export type actionsInterface = AddTask | EditTask | DeleteTask |LoadImages | PostImages | LoadPosts | PostPosts | SaveImage;
