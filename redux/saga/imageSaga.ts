import {takeEvery, call, put} from 'redux-saga/effects';
import {LOAD_IMAGES, POST_IMAGES, LoadImages} from '../types';
import {KEY} from '../../env';

interface Request {
  hits:[]
}

export function* imageSagaWatcher() {
  yield takeEvery(LOAD_IMAGES, sagaWorker);
}

function* sagaWorker({payload}:LoadImages) {
  const request:Request = yield call(()=>getPhoto(payload));
  yield put({type: POST_IMAGES, payload:request.hits});
}

async function getPhoto(query:string) {
  const response = await fetch(
    `https://pixabay.com/api/?key=${KEY}&q=${query}&image_type=photo`,
  );
  return await response.json();
}