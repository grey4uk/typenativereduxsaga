import {takeEvery, call, put} from 'redux-saga/effects';
import {LOAD_POSTS, POST_POSTS} from '../types';
import {firestore} from '../../firebase/config';

export function* postsSagaWatcher() {
  yield takeEvery(LOAD_POSTS, sagaWorker);
}

function* sagaWorker(){
  const request = yield call(getPosts);
  yield put({type: POST_POSTS, payload:request});
}

async function getPosts () {
   const respond = await firestore
    .collection('images')
    .get()
    .then((data) => {return data.docs.map((doc) => 
          ({ ...doc.data(), id: doc.id })   
      );
    });
    return await respond;
}
