import { all, fork } from 'redux-saga/effects';
import {imageSagaWatcher} from './imageSaga';
import {postsSagaWatcher} from './postSaga';
import { saveSagaWatcher } from './saveSaga';

export function* rootSaga () {
    yield all([
        fork(imageSagaWatcher), // saga1 can also yield [ fork(actionOne), fork(actionTwo) ]
        fork(postsSagaWatcher),
        fork(saveSagaWatcher)
    ]);
}