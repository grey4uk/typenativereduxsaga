import {takeEvery, call, put} from 'redux-saga/effects';
import {SAVE_IMAGE, SaveImage} from '../types';
import {firestore} from '../../firebase/config';

export function* saveSagaWatcher() {
  yield takeEvery(SAVE_IMAGE, sagaWorker);
}

function* sagaWorker({payload}:SaveImage) {
  yield call(()=>savePhoto(payload));
}

async function savePhoto(image:string) {
    await firestore.collection("images").add({
        image  
      });
}