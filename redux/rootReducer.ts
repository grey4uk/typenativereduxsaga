import {combineReducers} from 'redux';
import {
  ADD_TASK,
  EDIT_TASK,
  DELETE_TASK,
  POST_IMAGES,
  POST_POSTS,
  Task,
  actionsInterface,
} from './types';

interface InitialState {
  tasks: Task[];
  images: string[];
  posts:[]
}

const initialState: InitialState = {tasks: [], images: [], posts: []};

const taskReducer = (
  state = initialState.tasks,
  action: actionsInterface,
): Task[] => {
  switch (action.type) {
    case ADD_TASK:
      return [...state, action.payload];
    case EDIT_TASK:
      return [
        ...state.map((el) =>
          action.payload.id === el.id ? {...el, text: action.payload.text} : el,
        ),
      ];
    case DELETE_TASK:
      return [...state.filter((el) => el.id !== action.payload)];

    default:
      return state;
  }
};

const imagesReducer = (
  state = initialState.images,
  action: actionsInterface,
): string[] => {
  switch (action.type) {
    case POST_IMAGES:
      return [...action.payload];

    default:
      return state;
  }
};

const postsReducer = (
  state = initialState.posts,
  action: actionsInterface,
): [] => {
  switch (action.type) {
    case POST_POSTS:
      return [...action.payload];

    default:
      return state;
  }
};

const rootReducer = combineReducers({taskReducer, imagesReducer, postsReducer});

export type RootState = ReturnType<typeof rootReducer>;

export default rootReducer;
