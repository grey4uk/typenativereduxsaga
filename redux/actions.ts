import { ADD_TASK, EDIT_TASK, DELETE_TASK, Task, actionsInterface, POST_IMAGES, LOAD_IMAGES, SAVE_IMAGE, POST_POSTS, LOAD_POSTS} from './types';

export const addTaskAction = (task: Task):actionsInterface => ({
  type: ADD_TASK,
  payload: task,
});

export const editTaskAction = (task: Task):actionsInterface => ({
  type: EDIT_TASK,
  payload: task,
});

export const delTaskAction = (id: number):actionsInterface => ({
  type: DELETE_TASK,
  payload: id,
});

export const loadImages=(value:string):actionsInterface=>({
  type: LOAD_IMAGES,
  payload:value,
})

export const postImages = (images:string[]):actionsInterface=>({
  type:POST_IMAGES,
  payload:images
})

export const saveImage = (image:string):actionsInterface=>({
  type:SAVE_IMAGE,
  payload:image
})

export const loadPosts=():actionsInterface=>({
  type: LOAD_POSTS
})

export const postPosts = (posts:[]):actionsInterface=>({
  type:POST_POSTS,
  payload:posts
})