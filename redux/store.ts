import {createStore, applyMiddleware} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import createSagaMiddleware from 'redux-saga';

import rootReducer from './rootReducer';
import { rootSaga } from './saga/rootSaga';


// const configureStore=()=>{
//     const store=createStore(reducer);
//     return store;
// }
const saga = createSagaMiddleware();

const store = createStore(
  rootReducer,
  composeWithDevTools(
    applyMiddleware(saga),
    // other store enhancers if any
  ),
);

saga.run(rootSaga);

export default store;
/////////////////////SAGA WITH TOOLKIT////////////////////////////////////
// import {configureStore,getDefaultMiddleware, applyMiddleware} from '@reduxjs/toolkit';
// import createSagaMiddleware from 'redux-saga';

// import rootReducer from './rootReducer';
// import { sagaWatcher } from './saga';

// // const configureStore=()=>{
// //     const store=createStore(reducer);
// //     return store;
// // }
// const saga:any = createSagaMiddleware();
// const sagaStore=applyMiddleware(saga);

// const store = configureStore({reducer:rootReducer,
//     middleware: getDefaultMiddleware(
//         sagaStore
//     )
  
//   });

// saga.run(sagaWatcher);

// export default store;
